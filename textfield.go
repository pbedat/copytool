package main

import (
	"fmt"

	"github.com/eiannone/keyboard"
	"github.com/logrusorgru/aurora"
)

type textField struct {
	placeholder string
	value       string
	onBlur      func()
	onChange    func(v string)
}

func (txt *textField) render() string {
	if txt.value == "" {
		return fmt.Sprintf("_ %s", aurora.Italic(aurora.Gray(16, txt.placeholder)))
	}
	return fmt.Sprintf("%s_", txt.value)
}

func (txt *textField) dispatch(key keyboard.Key, r rune) {
	switch key {
	case keyboard.KeyBackspace:
		fallthrough
	case keyboard.KeyBackspace2:
		if len(txt.value) > 0 {
			txt.value = txt.value[0 : len(txt.value)-1]
		}
	case keyboard.KeyEnter:
		txt.onChange(txt.value)
	default:
		txt.value = fmt.Sprintf("%s%c", txt.value, r)
	}
}
