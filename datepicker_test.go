package main

import (
	"fmt"
	"testing"

	"github.com/logrusorgru/aurora"
)

func TestRender(t *testing.T) {
	dp := &datePicker{
		day:   1,
		month: 6,
		year:  2020,
		focus: inputMonth,
	}

	got := dp.render()
	want := fmt.Sprintf("%s 01.%s.2020", aurora.Gray(16, "Mo"), aurora.BgCyan("06").Gray(1))

	if got != want {
		t.Errorf("expected '%s' but got '%s'", want, got)
	}

	dp.cursorLeft()
	got = dp.render()
	want = fmt.Sprintf("%s %s.06.2020", aurora.Gray(16, "Mo"), aurora.BgCyan("01").Gray(1))

	if got != want {
		t.Errorf("expected '%s' but got '%s'", want, got)
	}

}

func TestCursor(t *testing.T) {
	dp := &datePicker{
		day:   1,
		month: 1,
		year:  2020,
		focus: inputDay,
	}

	dp.cursorDown()

	if dp.day != 31 {
		t.Errorf("expected '31' but got '%d'", dp.day)
	}

	dp.cursorUp()
	dp.cursorUp()

	if dp.day != 2 {
		t.Errorf("expected '2' but got '%d'", dp.day)
	}

	dp.cursorRight()
	dp.cursorDown()

	if dp.month != 12 {
		t.Errorf("expected '12' but got '%d'", dp.month)
	}

	dp.cursorUp()

	if dp.month != 1 {
		t.Errorf("expected '1' but got '%d'", dp.month)
	}
}
