module gitlab.com/pbedat/copytool

go 1.14

require (
	github.com/360EntSecGroup-Skylar/excelize/v2 v2.4.0 // indirect
	github.com/ahmetalpbalkan/go-cursor v0.0.0-20131010032410-8136607ea412
	github.com/eiannone/keyboard v0.0.0-20200508000154-caf4b762e807
	github.com/fatih/color v1.9.0
	github.com/goodsign/monday v1.0.0
	github.com/logrusorgru/aurora v0.0.0-20200102142835-e9ef32dff381
)
