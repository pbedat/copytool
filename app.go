package main

import (
	"fmt"
	"time"

	"github.com/eiannone/keyboard"
	"github.com/fatih/color"
	"github.com/logrusorgru/aurora"
)

type focusComp = string

const (
	fromComp    focusComp = "from"
	toComp      focusComp = "to"
	fileComp    focusComp = "file"
	confirmComp focusComp = "confirm"
	cellComp    focusComp = "cellComp"
)

type appState struct {
	file  string
	from  time.Time
	to    time.Time
	job   copyJob
	tasks copyTasks
	done  bool
}

type appComp struct {
	state appState

	from    *datePicker
	to      *datePicker
	file    *fileSelector
	confirm *confirm
	cell    *textField

	focus string
}

func (app *appComp) dispatch(key keyboard.Key, r rune) {

	switch app.focus {
	case fileComp:
		app.file.dispatch(key)
	case fromComp:
		app.from.dispatch(key)
	case toComp:
		app.to.dispatch(key)
	case confirmComp:
		app.confirm.dispatch(key, r)
	case cellComp:
		app.cell.dispatch(key, r)
	}
}

func (app *appComp) render() string {

	if app.focus == fileComp {
		return app.file.render()
	}

	if app.focus == fromComp {
		str := app.file.render()
		str += fmt.Sprintln(color.GreenString("?"), aurora.Bold("Von:  "), app.from.render())

		initial := time.Time{}

		if app.state.to != initial {
			str += fmt.Sprintln()
			str += fmt.Sprintln("✓", "Bis:  ", app.to.render())
		}
		str += fmt.Sprintln()
		str += fmt.Sprintln(aurora.Gray(9, "CURSOR: Wert verändern | TAB: Feld wechseln"))

		return str
	}

	str := app.file.render()
	str += fmt.Sprintln("✓", aurora.Bold("Von:  "), app.from.render())

	if app.focus == toComp {
		str += fmt.Sprint(color.GreenString("? "), aurora.Bold("Bis:   "), app.to.render())
		str += aurora.Gray(9, fmt.Sprintf(" (%d Tage)", int(app.to.Date().Sub(app.from.Date()).Hours()/float64(24.0))+1)).String()
		str += fmt.Sprintln() + fmt.Sprintln()
		str += fmt.Sprintln(aurora.Gray(9, "CURSOR: Wert verändern | TAB: Feld wechseln"))
		str += fmt.Sprintln()
	} else {
		str += fmt.Sprintln("✓", aurora.Bold("Bis:  "), app.to.render())

	}

	if app.focus == cellComp {
		str += fmt.Sprintln(color.GreenString("?"), aurora.Bold("Zelle:"), app.cell.render())
	} else {
		str += fmt.Sprintln("✓", aurora.Bold("Zelle:"), app.cell.value)
		str += app.confirm.render()
	}

	return str
}
