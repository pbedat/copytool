package main

import (
	"fmt"
	"io/ioutil"

	"github.com/eiannone/keyboard"
	"github.com/logrusorgru/aurora"
)

type fileSelector struct {
	selected int
	files    []string
	focus    bool

	onChange func(file string)
}

func newFileSelector(dir string) (*fileSelector, error) {

	entries, err := ioutil.ReadDir(dir)

	if err != nil {
		return nil, err
	}

	files := []string{}

	for _, f := range entries {
		if !f.IsDir() {
			files = append(files, f.Name())
		}
	}

	return &fileSelector{
		files: files,
	}, nil
}

func (fs *fileSelector) render() string {

	if !fs.focus {
		return fmt.Sprintf("%s %s: %s%s", "✓", aurora.Bold("Datei"), fs.files[fs.selected], fmt.Sprintln())
	}

	str := fmt.Sprintf("%s %s:", aurora.Green("?"), aurora.Bold("Datei"))
	str += fmt.Sprintln()
	str += fmt.Sprintln()

	for i, f := range fs.files {
		if i == fs.selected {
			str += aurora.Cyan("> ").String()
			str += fmt.Sprintln(f)
		} else {
			str += fmt.Sprintf("  %s", f)
			str += fmt.Sprintln()
		}
	}

	return str
}

func (fs *fileSelector) dispatch(key keyboard.Key) {

	switch key {
	case keyboard.KeyArrowDown:
		fs.selected++
		if fs.selected >= len(fs.files) {
			fs.selected = len(fs.files) - 1
		}
	case keyboard.KeyArrowUp:
		fs.selected--
		if fs.selected < 0 {
			fs.selected = 0
		}
	case keyboard.KeyEnter:
		fs.onChange(fs.files[fs.selected])
	}

}
