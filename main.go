package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"path"
	"path/filepath"
	"time"

	"github.com/360EntSecGroup-Skylar/excelize/v2"
	"github.com/ahmetalpbalkan/go-cursor"
	"github.com/eiannone/keyboard"
	"github.com/logrusorgru/aurora"
)

func main() {

	settings := CopyToolSettings{
		LastUsedCell: "A1",
	}

	err := settings.load()

	if err != nil {
		log.Println(err)
	}

	dir, err := os.Getwd()

	if err != nil {
		dir = os.TempDir()
	}

	flag.StringVar(&dir, "dir", dir, "Sets the working directory (default cwd)")
	flag.Parse()

	// component declaration

	fs, err := newFileSelector(dir)
	fs.focus = true

	if err != nil {
		panic(err)
	}

	from := &datePicker{
		day:   time.Now().Day(),
		month: int(time.Now().Month()),
		year:  time.Now().Year(),
		focus: inputDay,
	}

	to := &datePicker{
		day:   time.Now().Day(),
		month: int(time.Now().Month()),
		year:  time.Now().Year(),
		focus: inputNone,
	}

	conf := &confirm{}

	cell := &textField{placeholder: "(z.B. 'A1')", value: settings.LastUsedCell}

	// wiring up the app

	app := &appComp{
		from:    from,
		to:      to,
		file:    fs,
		confirm: conf,
		focus:   fileComp,
		cell:    cell,
	}

	from.onBlur = func() {
		app.focus = toComp
	}

	from.onChange = func(t time.Time) {
		app.focus = toComp
		app.to.focus = inputDay
		app.to.day = app.from.day
		app.to.month = app.from.month
		app.to.year = app.from.year
		app.state.from = app.from.Date()
	}

	to.onBlur = func() {
		app.focus = fromComp
	}

	to.onChange = func(t time.Time) {
		app.state.to = t

		app.to.focus = inputNone

		app.focus = cellComp
	}

	cell.onChange = func(value string) {
		if value == "" {
			value = "A1"
		}
		app.focus = confirmComp

		job := copyJob{
			file: app.state.file,
			from: app.state.from,
			to:   app.state.to,
			cell: value,
		}

		app.state.job = job
		app.state.tasks = job.tasks()

		app.confirm.tasks = app.state.tasks
	}

	conf.onSubmit = func(ok bool) {
		app.state.done = true
		if ok {
			for _, task := range app.state.tasks {
				if task.taskType == taskDir {
					fmt.Println("Erstelle Ordner", task.path, "...")
					fmt.Println()
					os.Mkdir(task.path, os.ModePerm)
				} else {
					fmt.Println("Kopiere Datei", task.path, "...")

					err := CopyFile(app.state.job.file, task.path)

					if err != nil {
						fmt.Println(err)
					}

					fmt.Println(task.date.Format("02.01.2006"))
					err = writeDateToFile(task.path, app.state.job.cell, task.date)

					if err != nil {
						fmt.Println("Datum konnte nicht geschrieben werden:", err)
					}
				}
			}

			fmt.Println()
			fmt.Println(aurora.Green("✓"), "Vorgang erfolgreich abgeschlossen")
			fmt.Println()

			settings.LastUsedCell = app.state.job.cell
			settings.save()

		} else {
			fmt.Println("Bye.")
			os.Exit(0)
		}
	}

	fs.onChange = func(file string) {
		app.state.file = path.Join(dir, file)
		app.focus = fromComp
		app.file.focus = false
	}

	// get the party started

	err = keyboard.Open()

	if err != nil {
		panic(err)
	}

	defer keyboard.Close()

	// ui loop
	for {
		// make sure there are no left over mutlinie renderings, when only one line is rendered
		fmt.Print(cursor.ClearScreenDown())

		fmt.Println(app.render())

		r, key, err := keyboard.GetSingleKey()

		if key == keyboard.KeyCtrlC {
			return
		}

		if err != nil {
			fmt.Println(err)
		}

		app.dispatch(key, r)

		if app.state.done {
			break
		}

		// restore the cursor position
		fmt.Print(cursor.MoveUpperLeft(0))
	}
}

func writeDateToFile(path string, cell string, date time.Time) error {
	f, err := excelize.OpenFile(path)

	if err != nil {
		return err
	}

	sheetId := f.WorkBook.Sheets.Sheet[0].Name

	err = f.SetCellValue(sheetId, cell, date.Format("02.01.2006"))

	if err != nil {
		return err
	}

	return f.Save()
}

type CopyToolSettings struct {
	LastUsedCell string
}

func (s *CopyToolSettings) load() error {
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))

	if err != nil {
		return err
	}

	settingsPath := path.Join(dir, ".copytool")

	f, err := os.OpenFile(settingsPath, os.O_CREATE|os.O_RDONLY, os.ModePerm)

	if err != nil {
		return err
	}

	defer f.Close()

	dec := json.NewDecoder(f)

	return dec.Decode(s)
}

func (s *CopyToolSettings) save() error {
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))

	if err != nil {
		log.Fatal(err)
	}

	settingsPath := path.Join(dir, ".copytool")

	f, err := os.OpenFile(settingsPath, os.O_CREATE|os.O_TRUNC|os.O_WRONLY, os.ModePerm)

	if err != nil {
		return err
	}

	defer f.Close()

	enc := json.NewEncoder(f)

	return enc.Encode(s)
}
