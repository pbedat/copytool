package main

import (
	"fmt"
	"os"
	"path"
	"time"
)

type copyJob struct {
	file string
	from time.Time
	to   time.Time
	cell string
}

type copyTaskType string

const (
	taskDir  copyTaskType = "dir"
	taskFile copyTaskType = "file"
)

type copyTask struct {
	taskType copyTaskType
	path     string
	version  int
	date     time.Time
}

type copyTasks []copyTask

func (job copyJob) tasks() copyTasks {

	filename := path.Base(job.file)
	extension := path.Ext(filename)
	name := filename[0 : len(filename)-len(extension)]
	basePath := job.file[0 : len(job.file)-len(filename)]

	tasks := []copyTask{}

	date := job.from

	for date.Before(job.to) || date.Sub(job.to) < (time.Hour*24*7) {
		_, week := date.ISOWeek()
		folder := path.Join(basePath, fmt.Sprintf("KW%02d", week))

		if _, err := os.Stat(folder); os.IsNotExist(err) {
			tasks = append(tasks, copyTask{
				taskType: taskDir,
				path:     folder,
				date:     date,
			})
		}

		date = date.AddDate(0, 0, 7)
	}

	date = job.from
	for date.Before(job.to) || date == job.to {
		_, week := date.ISOWeek()
		folder := path.Join(basePath, fmt.Sprintf("KW%02d", week))
		version := 1

		var file string

		for {
			file = path.Join(folder, fmt.Sprintf("%s_%s_V%d%s", date.Format("20060102"), name, version, extension))
			if _, err := os.Stat(file); os.IsNotExist(err) {
				break
			}
			version++
		}

		tasks = append(tasks, copyTask{
			taskType: taskFile,
			path:     file,
			version:  version,
			date:     date,
		})

		date = date.AddDate(0, 0, 1)
	}

	return tasks
}
