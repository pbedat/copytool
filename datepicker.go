package main

import (
	"fmt"
	"strings"
	"time"

	"github.com/eiannone/keyboard"
	"github.com/goodsign/monday"
	"github.com/logrusorgru/aurora"
)

type input = int

const (
	inputNone  input = -1
	inputDay   input = 0
	inputMonth input = 1
	inputYear  input = 2
)

type datePicker struct {
	year  int
	month int
	day   int

	focus input

	onBlur   func()
	onChange func(d time.Time)
}

func (dp *datePicker) cursorLeft() {
	focus := dp.focus - 1

	if focus < 0 {
		dp.focus = 0
	} else {
		dp.focus = focus
	}
}

func (dp *datePicker) cursorRight() {
	focus := dp.focus + 1

	if focus > inputYear {
		dp.focus = inputYear
	} else {
		dp.focus = focus
	}
}

func (dp datePicker) getCursor() int {
	switch dp.focus {
	case inputDay:
		return 0
	case inputMonth:
		return 3
	case inputYear:
		return 6
	}

	panic(fmt.Errorf("unknown input %v", dp.focus))
}

func (dp datePicker) render() string {
	segments := []string{
		fmt.Sprintf("%02d", dp.day),
		fmt.Sprintf("%02d", dp.month),
		fmt.Sprintf("%d", dp.year),
	}

	for i, segment := range segments {
		if i == dp.focus {
			segments[i] = aurora.Gray(1, segment).BgCyan().String()
		}
	}

	dateStr := strings.Join(segments, ".")

	date := dp.Date()
	prefix := monday.Format(date, "Mon", monday.LocaleDeDE)

	return fmt.Sprintf("%s %s", aurora.Gray(16, prefix), dateStr)
}

func (dp datePicker) Date() time.Time {
	return time.Date(dp.year, time.Month(dp.month), dp.day, 0, 0, 0, 0, time.UTC)
}

func (dp *datePicker) cursorUp() {

	switch dp.focus {

	case inputDay:
		daysInMonth := time.Date(dp.Date().Year(), dp.Date().Month()+1, 0, 0, 0, 0, 0, time.UTC).Day()

		if dp.day == daysInMonth {
			dp.day = 1
		} else {
			dp.day++
		}

	case inputMonth:
		if dp.month == 12 {
			dp.month = 1
		} else {
			dp.month++
		}

	case inputYear:
		dp.year++
	}

}

func (dp *datePicker) cursorDown() {

	switch dp.focus {

	case inputDay:
		daysInMonth := time.Date(dp.Date().Year(), dp.Date().Month()+1, 0, 0, 0, 0, 0, time.UTC).Day()

		if dp.day == 1 {
			dp.day = daysInMonth
		} else {
			dp.day--
		}

	case inputMonth:
		if dp.month == 1 {
			dp.month = 12
		} else {
			dp.month--
		}

	case inputYear:
		dp.year--
	}

}

func (dp *datePicker) onFocus() {
	dp.focus = inputDay
}

func (dp *datePicker) dispatch(key keyboard.Key) {
	switch key {
	case keyboard.KeyArrowDown:
		dp.cursorDown()
	case keyboard.KeyArrowUp:
		dp.cursorUp()
	case keyboard.KeyArrowLeft:
		dp.cursorLeft()
	case keyboard.KeyArrowRight:
		dp.cursorRight()
	case keyboard.KeyEnter:
		dp.focus = inputNone
		dp.onChange(dp.Date())
	case keyboard.KeyTab:
		dp.focus = inputNone
		dp.onBlur()
	}

}
