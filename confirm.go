package main

import (
	"fmt"

	"github.com/eiannone/keyboard"
	"github.com/logrusorgru/aurora"
)

type confirm struct {
	tasks copyTasks

	onSubmit func(ok bool)
}

func (c confirm) render() string {

	folders := 0
	files := 0

	for _, t := range c.tasks {
		if t.taskType == taskDir {
			folders++
		} else {
			files++
		}
	}

	return fmt.Sprintln(
		aurora.Green("?"),
		fmt.Sprintf("%d Ordner und %d Dateien erstellen? [J/n]", folders, files),
	)
}

func (c confirm) dispatch(key keyboard.Key, r rune) {
	if r == 'j' || key == keyboard.KeyEnter {
		c.onSubmit(true)
	}

	if r == 'n' {
		c.onSubmit(false)
	}
}
